const num = 8;
const getCube = Math.pow(num,3);
let message = `The cube of ${num} is ${getCube}.`
console.log(message);

let address = ["23", "Begonia Street", "Roxas District", "Quezon City", "Philippines", "1103"]

let [number, street, barangay, city, country, zipCode] = address
message = `I live at ${number} ${street} ${barangay} ${city}, ${country} ${zipCode}.`;
console.log(message);

let animal = {
	name: "Lolong",
	type: "crocodile",
	weight: "1075kgs",
	measurement: "20ft and 3in"
}

let {name, type, weight, measurement} = animal;
message = `${name} was a saltwater ${type}. He weighed ${weight} with a measurement of ${measurement}.`
console.log(message)

let array = [1, 2, 3, 4, 5]
let each = array.forEach(index => console.log(index))
	
let reduceNumber = array.reduce((x,y) => x+y)
console.log(reduceNumber)

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	} 
}
let dog1 = new Dog("Abdul", 2, "Pomeranian");
console.log(dog1)